** START **

** Build/Run **
Open Eclipse IDE.
Import Existing Maven Project.
Run Project.

** Service via Rest API **
Run Project to Preload Database
Register a drone: http://localhost:8080/drones, http//localhost:8080/drones/{id}
Loading a drone with medication: http://localhost:8080/terminals, http//localhost:8080/terminals/{id}
Checking medication items for a given drone: http://localhost:8080/medications, http//localhost:8080/medicatons/{id}
Checking available drones for loading: http//localhost:8080/drones
Checking drone battery level: http://localhost:8080/drones/{id}

** END **

