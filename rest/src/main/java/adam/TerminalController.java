package adam;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TerminalController {

	private final TerminalRepository repository;

	TerminalController(TerminalRepository repository) {
		this.repository = repository;
	}

	// Aggregate root

	// tag::get-aggregate-root[]
	@GetMapping("/terminals")
	CollectionModel<EntityModel<Terminal>> all() {

		List<EntityModel<Terminal>> terminals = repository.findAll().stream()
				.map(terminal -> EntityModel.of(terminal,
						linkTo(methodOn(TerminalController.class).one(terminal.getId())).withSelfRel(),
						linkTo(methodOn(TerminalController.class).all()).withRel("terminals")))
				.collect(Collectors.toList());
		
		// Writer file
		File rootFile = new File("terminal-log.txt");
		if (rootFile.exists()) {
			try {
				FileWriter mWriter = new FileWriter((rootFile));
				
				//PrintWriter mPrinter = new PrintWriter(mWriter);
				mWriter.write(terminals.toString());
				mWriter.flush();
				mWriter.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//CSVPrinter mPrinter = new CSVPrinter();
		}

		return CollectionModel.of(terminals, linkTo(methodOn(TerminalController.class).all()).withSelfRel());
	}
	// end::get-aggregate-root[]

	@PostMapping("/add-terminal-activity")
	Terminal newTerminal(@RequestBody Terminal newTerminal) {
		return repository.save(newTerminal);
	}

	// Single item

	// tag::get-single-item[]
	@GetMapping("/terminals/{id}")
	EntityModel<Terminal> one(@PathVariable Long id) {

		Terminal terminal = repository.findById(id) //
				.orElseThrow(() -> new TerminalActivityNotFoundException(id));

		return EntityModel.of(terminal, //
				linkTo(methodOn(TerminalController.class).one(id)).withSelfRel(),
				linkTo(methodOn(TerminalController.class).all()).withRel("terminals"));
	}
	// end::get-single-item[]

	@PutMapping("/update-terminal-activity/{id}")
	Terminal replaceTerminal(@RequestBody Terminal newTerminal, @PathVariable Long id) {

		return repository.findById(id) //
				.map(terminal -> {
					terminal.setDroneSerialNo(newTerminal.getDroneSerialNo());
					terminal.setMedCode(newTerminal.getMedCode());
					terminal.setLuggage(newTerminal.getLuggage());;
					terminal.setOrigin(newTerminal.getOrigin());
					terminal.setDestination(newTerminal.getDestination());
					terminal.setStatus(newTerminal.getStatus());;
					return repository.save(terminal);
				}) //
				.orElseGet(() -> {
					newTerminal.setId(id);
					return repository.save(newTerminal);
				});
	}

	@DeleteMapping("/delete-terminal-activity/{id}")
	void deleteTerminal(@PathVariable Long id) {
		repository.deleteById(id);
	}
	
}
