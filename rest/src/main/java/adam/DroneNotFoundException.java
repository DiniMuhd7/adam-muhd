package adam;

class DroneNotFoundException extends RuntimeException {

	DroneNotFoundException(Long id) {
		super("Could not find drone:  " + id);
	}
}
