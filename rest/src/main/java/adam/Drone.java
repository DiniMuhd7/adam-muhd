package adam;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
class Drone {
	
	private @Id @GeneratedValue Long id;
	
	private String sNo;
	private String model;
	private double weight;
	private double battery;
	private String state;

	Drone() {}

	Drone(String sNo, String model, double weight, double battery, String state) {
		if (!sNo.trim().isEmpty() && !model.trim().isEmpty() && weight != 0 && battery != 0 && !state.trim().isEmpty()) {
			if (sNo.trim().length() <= 100) { 
				if (model.trim().equalsIgnoreCase("Lightweight") || model.trim().equalsIgnoreCase("Middleweight")
						|| model.trim().equalsIgnoreCase("Cruiserweight") || model.trim().equalsIgnoreCase("Heavyweight")) {
					if (weight <= 500) {
						this.sNo = sNo;
						this.model = model;
						this.weight = weight;
						this.battery = battery;
						this.state = state;
					} else {
						System.out.println("Drone weight cannot be more than 500gr");
						System.exit(0);
					}
				} else {
					System.out.println("The specified model of drone is invalid and not supported");
					System.exit(0);
				}
			} else {
				System.out.println("Drone SerialNo cannot be more than 100 characters");
				System.exit(0);
			}
		} else {
			System.out.println("You must enter all information to add a new drone.");
			System.exit(0);
		}
		
	}

	public Long getId() {return this.id;}
	public String getSerialNo() {return this.sNo;}
	public String getModel() {return this.model;}
	public double getWeight() {return this.weight;}
	public double getBattery() {return this.battery;}
	public String getState() {return this.state;}

	public void setId(Long id) {this.id = id;}
	public void setSerialNo(String sNo) {this.sNo = sNo;}
	public void setModel(String model) {this.model = model;}
	public void setWeight(double weight) {this.weight = weight;}
	public void setBattery(double battery) {this.battery = battery;}
	public void setState(String state) {this.state = state;}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof Drone))
			return false;
		
		Drone drone = (Drone) o;
		return Objects.equals(this.id, drone.id) && Objects.equals(this.sNo, drone.sNo)
				&& Objects.equals(this.model, drone.model) && Objects.equals(this.weight, drone.weight)
				&& Objects.equals(this.battery, drone.battery) && Objects.equals(this.state, drone.state);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.sNo, this.model, this.weight, this.battery, this.state);
	}

	@Override
	public String toString() {
		return "Drone{" + "id=" + this.id + ", SerialNo='" + this.sNo + '\'' + ", Model='" + this.model + '\'' 
				+ ", Weight='" + this.weight + '\'' + ", Battery='" + this.battery + '\''
				+ ", State='" + this.state + '\'' + '}';
	}
}
