package adam;

import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Terminal {

	private @Id @GeneratedValue Long id;
	
	private String dSerialNo;
	private String medCode;
	private Double mLuggage;
	private String origin;
	private String destination;
	private String status;

	Terminal() {}

	Terminal(String dSerialNo, String medCode, Double mLuggage, String origin, String destination, String status) {
		if (!dSerialNo.trim().isEmpty() && !medCode.trim().isEmpty() && mLuggage != 0 && !origin.trim().isEmpty()
				&& !destination.trim().isEmpty() && !status.trim().isEmpty()) {
			this.dSerialNo = dSerialNo;
			this.medCode = medCode;
			this.mLuggage = mLuggage;
			this.origin = origin;
			this.destination = destination;
			this.status = status;
		} else {
			System.out.println("You must enter all information to add a new terminal activity");
			System.exit(0);
		}
		
	}

	public Long getId() {return this.id;}
	public String getDroneSerialNo() {return this.dSerialNo;}
	public String getMedCode() {return this.medCode;}
	public Double getLuggage() {return this.mLuggage;}
	public String getOrigin() {return this.origin;}
	public String getDestination() {return this.destination;}
	public String getStatus() {return this.status;}
	
	public void setId(Long id) {this.id = id;}
	public void setDroneSerialNo(String dSerialNo) {this.dSerialNo = dSerialNo;}
	public void setMedCode(String medCode) {this.medCode = medCode;}
	public void setLuggage(Double mLuggage) {this.mLuggage = mLuggage;}
	public void setOrigin(String origin) {this.origin = origin;}
	public void setDestination(String destination) {this.destination = destination;}
	public void setStatus(String status) {this.status = status;}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof Terminal))
			return false;
		
		Terminal terminal = (Terminal) o;
		return Objects.equals(this.id, terminal.id) && Objects.equals(this.dSerialNo, terminal.dSerialNo)
				&& Objects.equals(this.medCode, terminal.medCode) && Objects.equals(this.mLuggage, terminal.mLuggage)
				&& Objects.equals(this.origin, terminal.origin) && Objects.equals(this.destination, terminal.destination)
				&& Objects.equals(this.status, terminal.status);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.dSerialNo, this.medCode, this.mLuggage, this.origin, this.destination, this.status);
	}

	@Override
	public String toString() {
		return "Terminal{" + "id=" + this.id + ", DroneSerialNo='" + this.dSerialNo + '\'' + ", MedicationCode='" + this.medCode + '\'' 
				+ ", Luggage='" + this.mLuggage + '\'' + ", Origin='" + this.origin + '\''
				+ ", Destination='" + this.destination + '\'' + ", Status='" + this.status + '}';
	}
	
}
