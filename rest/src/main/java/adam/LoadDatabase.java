package adam;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);
	
	private List<String> medCode = new ArrayList<String>();
	private List<Double> mLuggage = new ArrayList<Double>();
	Double[] dLuggage = null;
	
	@Bean
	CommandLineRunner initDatabaseDrone(DroneRepository repository) {
		return args -> {
			log.info("Preloading " + repository.save(new Drone("DRN-252077", "Lightweight",10.7,78.9,"IDLE")));
			log.info("Preloading " + repository.save(new Drone("DRN-252055", "Middleweight",15.7,70.7,"IDLE")));
			log.info("Preloading " + repository.save(new Drone("DRN-252047", "Cruiserweight",17.7,68.5,"IDLE")));
			log.info("Preloading " + repository.save(new Drone("DRN-252022", "Heavyweight",20.7,50.2,"IDLE")));
			log.info("Preloading " + repository.save(new Drone("DRN-252077", "Lightweight",10.7,78.9,"IDLE")));
			log.info("Preloading " + repository.save(new Drone("DRN-252055", "Middleweight",15.7,70.7,"IDLE")));
			log.info("Preloading " + repository.save(new Drone("DRN-252047", "Cruiserweight",17.7,68.5,"IDLE")));
			log.info("Preloading " + repository.save(new Drone("DRN-252022", "Heavyweight",20.7,50.2,"IDLE")));
			log.info("Preloading " + repository.save(new Drone("DRN-252022", "Heavyweight",20.7,50.2,"IDLE")));
			log.info("Preloading " + repository.save(new Drone("DRN-252077", "Lightweight",10.7,78.9,"IDLE")));
		};
	}
	
	
	@Bean
	CommandLineRunner initDatabaseMedication(MedicationRepository repository) {

		return args -> {
			log.info("Preloading " + repository.save(new Medication("MED-T20METY", 5.7, "MED-T20METY","https://bitbucket.com/med-225077")));
			log.info("Preloading " + repository.save(new Medication("MED-AB10LIZ", 12.7, "MED-AB10LIZ", "https://bitbucket.com/med-225077")));
			log.info("Preloading " + repository.save(new Medication("MED-SH12PSY", 10.7, "MED-SH12PSY","https://bitbucket.com/med-225077")));
			log.info("Preloading " + repository.save(new Medication("MED-P70COLD", 8.7, "MED-P70COLD","https://bitbucket.com/med-225077")));
			log.info("Preloading " + repository.save(new Medication("MED-T20METY", 5.7, "MED-T20METY","https://bitbucket.com/med-225077")));
		};
	}
	
	
	@Bean
	CommandLineRunner initDatabaseTerminal(TerminalRepository repository, DroneRepository dRepository) {
		
		medCode.add("MED-225087");
		medCode.add("MED-225077");
		medCode.add("MED-225087");
		medCode.add("MED-225077");
		
		mLuggage.add(12.7);
		mLuggage.add(12.7);
		mLuggage.add(12.7);
		mLuggage.add(12.7);
		
		
		//dLuggage = (Double[]) mLuggage.toArray();
		Double tLuggage = mLuggage.get(0) + mLuggage.get(1) + mLuggage.get(2) + mLuggage.get(3);
		Terminal t1 = new Terminal("DRN-252077", medCode.toString(), tLuggage, "Zaria", "Abuja", "Completed");
		Terminal t2 = new Terminal("DRN-252077", medCode.toString(), tLuggage, "Zaria", "Abuja", "Completed");
		Terminal t3 = new Terminal("DRN-252077", medCode.toString(), tLuggage, "Zaria", "Abuja", "Completed");
		Terminal t4 = new Terminal("DRN-252077", medCode.toString(), tLuggage, "Zaria", "Abuja", "Completed");
	
		return args -> {
			
			log.info("Preloading " + repository.save(t1));
			log.info("Preloading " + repository.save(t2));
			log.info("Preloading " + repository.save(t3));
			log.info("Preloading " + repository.save(t4));
		};
	}
	
	
	
}
