package adam;

public class TerminalActivityNotFoundException extends RuntimeException {

	TerminalActivityNotFoundException(Long id) {
		super("Could not find terminal activity:  " + id);
	}
	
}
