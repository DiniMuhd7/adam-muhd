package adam;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MedicationController {

	private final MedicationRepository repository;

	MedicationController(MedicationRepository repository) {
		this.repository = repository;
	}

	// Aggregate root

	// tag::get-aggregate-root[]
	@GetMapping("/medications")
	CollectionModel<EntityModel<Medication>> all() {

		List<EntityModel<Medication>> medications = repository.findAll().stream()
				.map(medication -> EntityModel.of(medication,
						linkTo(methodOn(MedicationController.class).one(medication.getId())).withSelfRel(),
						linkTo(methodOn(MedicationController.class).all()).withRel("medications")))
				.collect(Collectors.toList());
		
		// Writer file
		File rootFile = new File("medication-log.txt");
		if (rootFile.exists()) {
			try {
				FileWriter mWriter = new FileWriter((rootFile));
				
				//PrintWriter mPrinter = new PrintWriter(mWriter);
				mWriter.write(medications.toString());
				mWriter.flush();
				mWriter.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//CSVPrinter mPrinter = new CSVPrinter();
		}

		return CollectionModel.of(medications, linkTo(methodOn(DroneController.class).all()).withSelfRel());
	}
	// end::get-aggregate-root[]

	@PostMapping("/addmedication")
	Medication newMedication(@RequestBody Medication newMedication) {
		return repository.save(newMedication);
	}

	// Single item

	// tag::get-single-item[]
	@GetMapping("/medications/{id}")
	EntityModel<Medication> one(@PathVariable Long id) {

		Medication medication = repository.findById(id) //
				.orElseThrow(() -> new MedicationNotFoundException(id));

		return EntityModel.of(medication, //
				linkTo(methodOn(MedicationController.class).one(id)).withSelfRel(),
				linkTo(methodOn(MedicationController.class).all()).withRel("medications"));
	}
	// end::get-single-item[]

	@PutMapping("/updatemedication/{id}")
	Medication replaceMedication(@RequestBody Medication newMedication, @PathVariable Long id) {

		return repository.findById(id) //
				.map(medication -> {
					medication.setName(newMedication.getName());
					medication.setWeight(newMedication.getWeight());
					medication.setCode(newMedication.getCode());
					medication.setImage(newMedication.getImage());
					return repository.save(medication);
				}) //
				.orElseGet(() -> {
					newMedication.setId(id);
					return repository.save(newMedication);
				});
	}

	@DeleteMapping("/delete-medication/{id}")
	void deleteMedication(@PathVariable Long id) {
		repository.deleteById(id);
	}
	
}
