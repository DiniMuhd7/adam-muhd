package adam;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

// tag::hateoas-imports[]
// end::hateoas-imports[]

@RestController
class DroneController {

	private final DroneRepository repository;

	DroneController(DroneRepository repository) {
		this.repository = repository;
	}

	// Aggregate root

	// tag::get-aggregate-root[]
	@GetMapping("/drones")
	CollectionModel<EntityModel<Drone>> all() {

		List<EntityModel<Drone>> drones = repository.findAll().stream()
				.map(drone -> EntityModel.of(drone, 
						linkTo(methodOn(DroneController.class).one(drone.getId())).withSelfRel(),
						linkTo(methodOn(DroneController.class).all()).withRel("drones")))
				.collect(Collectors.toList());
		
		// Writer file
		File rootFile = new File("drones-log.txt");
		if (rootFile.exists()) {
			try {
				FileWriter mWriter = new FileWriter((rootFile));
				
				//PrintWriter mPrinter = new PrintWriter(mWriter);
				mWriter.write(drones.toString());
				mWriter.flush();
				mWriter.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//CSVPrinter mPrinter = new CSVPrinter();
		}
		
		return CollectionModel.of(drones, linkTo(methodOn(DroneController.class).all()).withSelfRel());
	}
	// end::get-aggregate-root[]

	@PostMapping("/drones/add")
	Drone newDrone(@RequestBody Drone newDrone) {
		return repository.save(newDrone);
	}

	// Single item

	// tag::get-single-item[]
	@GetMapping("/drones/{id}")
	EntityModel<Drone> one(@PathVariable Long id) {

		Drone drone = repository.findById(id) //
				.orElseThrow(() -> new DroneNotFoundException(id));

		return EntityModel.of(drone, //
				linkTo(methodOn(DroneController.class).one(id)).withSelfRel(),
				linkTo(methodOn(DroneController.class).all()).withRel("drones"));
	}
	// end::get-single-item[]
	
	
	// tag::get-aggregate-root[]
		@GetMapping("/drones/load")
		CollectionModel<EntityModel<Drone>> load() {

			List<EntityModel<Drone>> drones = repository.findAll().stream()
					.map(drone -> EntityModel.of(drone, 
							linkTo(methodOn(DroneController.class).one(drone.getId())).withSelfRel(),
							linkTo(methodOn(DroneController.class).load()).withRel("drones/load")))
					.collect(Collectors.toList());
			
			return CollectionModel.of(drones, linkTo(methodOn(DroneController.class).load()).withSelfRel());
		}
		// end::get-aggregate-root[]

		
	@PutMapping("/drones/update/{id}")
	Drone replaceDrone(@RequestBody Drone newDrone, @PathVariable Long id) {

		return repository.findById(id) //
				.map(drone -> {
					drone.setSerialNo(newDrone.getSerialNo());
					drone.setModel(newDrone.getModel());
					drone.setWeight(newDrone.getWeight());
					drone.setBattery(newDrone.getBattery());
					drone.setState(newDrone.getState());
					return repository.save(drone);
				}) //
				.orElseGet(() -> {
					newDrone.setId(id);
					return repository.save(newDrone);
				});
	}

	@DeleteMapping("/drones/delete/{id}")
	void deleteDrone(@PathVariable Long id) {
		repository.deleteById(id);
	}
}
