package adam;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public class TerminalActivityNotFoundAdvice {
	@ResponseBody
	@ExceptionHandler(TerminalActivityNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String terminalNotFoundHandler(TerminalActivityNotFoundException ex) {
		return ex.getMessage();
	}
}
