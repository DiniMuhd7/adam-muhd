package adam;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Medication {
	
		private @Id @GeneratedValue Long id;
		
		private String mName;
		private double weight;
		private String code;
		private String image;

		Medication() {}

		Medication(String mName, double weight, String code, String image) {
			Pattern pName = Pattern.compile("^[a-zA-Z0-9_-]*$");
			Pattern pCode = Pattern.compile("^[a-zA-Z0-9_-]*$");
			if (!mName.trim().isEmpty() && weight != 0 && !code.trim().isEmpty() && !image.trim().isEmpty()) {
				Matcher pMatcher = pName.matcher(mName);
				if (pMatcher.find()) {
					Matcher cMatcher = pCode.matcher(code);
					if (cMatcher.find()) {
						this.mName = mName;
						this.weight = weight;
						this.code = code;
						this.image = image;
					} else {
						System.out.println("Medication code name must contain only upper letters, numbers, underscores");
						System.exit(0);
					}
				} else {
					System.out.println("Medication name must contain only letters, numbers, ‘-‘, ‘_’");
					System.exit(0);
				}
			} else {
				System.out.println("You must enter all information to add a new medication");
				System.exit(0);
			}
		}

		public Long getId() {return this.id;}
		public String getName() {return this.mName;}
		public double getWeight() {return this.weight;}
		public String getCode() {return this.code;}
		public String getImage() {return this.image;}

		public void setId(Long id) {this.id = id;}
		public void setName(String mName) {this.mName = mName;}
		public void setWeight(double weight) {this.weight = weight;}
		public void setCode(String code) {this.code = code;}
		public void setImage(String image) {this.image = image;}

		@Override
		public boolean equals(Object o) {

			if (this == o)
				return true;
			if (!(o instanceof Drone))
				return false;
			
			Medication medication = (Medication) o;
			return Objects.equals(this.id, medication.id) && Objects.equals(this.mName, medication.mName)
					&& Objects.equals(this.weight, medication.weight) && Objects.equals(this.code, medication.code)
					&& Objects.equals(this.image, medication.image);
		}

		@Override
		public int hashCode() {
			return Objects.hash(this.id, this.mName, this.weight, this.code, this.image);
		}

		@Override
		public String toString() {
			return "Medication{" + "id=" + this.id + ", Name='" + this.mName + '\'' + ", Weight='" + this.weight + '\'' 
					+ ", Code='" + this.code + '\'' + ", Image='" + this.image + '\'' + '}';
		}
	
}
